package tax;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Main {


    static final int REGIONS_NUMBER = 89;

    static final double VAT = 0.18;
    static final int DATA_SET_SIZE = 1_000_000;

    static final double SUM_ERROR_PERCENT = 0.05;
    static final double VAT_ERROR_PERCENT = 0.02;
    static final double INN_ERROR_PERCENT = 0.01;

    static final int SUM_ERROR_PORTION = (int)Math.round(1.0 / SUM_ERROR_PERCENT);
    static final int VAT_ERROR_PORTION = (int)Math.round(1.0 / VAT_ERROR_PERCENT);
    static final int INN_ERROR_PORTION = (int)Math.round(1.0 / INN_ERROR_PERCENT);


    private static String generateINN() {
        ArrayList<Integer> INN = new ArrayList<>();

        Random random = new Random();

        // 1--2
        int region = random.nextInt(REGIONS_NUMBER) + 1;
        INN.add(region / 10);
        INN.add(region % 10);

        // 3--4
        int inspection = random.nextInt(99) + 1;
        INN.add(inspection / 10);
        INN.add(inspection % 10);

        // 5--9
        for(int i = 5; i <= 9; ++i) {
            INN.add(random.nextInt(9));
        }

        int controlSum = 2*INN.get(0) + 4*INN.get(1) + 10*INN.get(2) +
                         3*INN.get(3) + 5*INN.get(4) +  9*INN.get(5) +
                         4*INN.get(6) + 6*INN.get(7) +  8*INN.get(8);
        int control = controlSum / 11;
        control *= 11;
        control = Math.abs(controlSum - control);
        if (control == 10) control = 0;
        INN.add(control);

        StringBuilder builder = new StringBuilder();
        for(int i : INN) {
            builder.append(i);
        }

        return builder.toString();
    }

    private static String generateKPP(String INN) {
        Random random = new Random();

        String KPP = INN.substring(0, 4);

        int pp = random.nextInt(49) + 1;
        if (pp < 10) KPP = KPP.concat("0");
        KPP = KPP.concat(Integer.toString(pp));

        KPP = KPP.concat(Integer.toString(random.nextInt(9)));
        KPP = KPP.concat(Integer.toString(random.nextInt(9)));
        KPP = KPP.concat(Integer.toString(random.nextInt(9)));

        return KPP;
    }

    static double generateTotal() {
        return 10_000_000.00 * new Random().nextDouble() + 50_000.00;
    }

    static void generateData() {
        Random random = new Random();

        int counterINN = 0;
        int counterSUM = 0;
        int counterVAT = 0;
        int counterTotalErrorRows = 0;
        int counterTypes[] = {0, 0, 0, 0};
        int counterTheSame = 0;

        boolean[] error = new boolean[3];
        boolean[] theSame = new boolean[3];

        int sumGapIndex = 0;
        int sumErrorIndex = random.nextInt(SUM_ERROR_PORTION);

        int vatGapIndex = 0;
        int vatErrorIndex = random.nextInt(VAT_ERROR_PORTION);

        int innGapIndex = 0;
        int innErrorIndex = random.nextInt(INN_ERROR_PORTION);

        try {
            BufferedWriter writerSeller   = new BufferedWriter(new FileWriter("seller.csv"));
            BufferedWriter writerCustomer = new BufferedWriter(new FileWriter("customer.csv"));

            writerSeller.append("seller_INN,seller_KPP,customer_INN,customer_KPP,total_without_VAT,total_with_VAT\n");
            writerCustomer.append("customer_INN,customer_KPP,seller_INN,seller_KPP,total_without_VAT,total_with_VAT\n");

            for(int i = 0; i < DATA_SET_SIZE;) {

                String sellerINN = generateINN();
                String sellerKPP = generateKPP(sellerINN);
                for(int j = 0; j < random.nextInt(5) + 1; ++j, ++i) {
                    String customerINN = generateINN();
                    String customerKPP = generateKPP(customerINN);

                    double total = generateTotal();
                    double totalVAT = total * (1.00 + VAT);

                    String sellerStr = new StringBuilder()
                            .append(sellerINN + ",").append(sellerKPP + ",")
                            .append(customerINN + ",").append(customerKPP + ",")
                            .append(String.format("%.2f,", total))
                            .append(String.format("%.2f", totalVAT))
                            .toString();

                    error[0] = false;
                    error[1] = false;
                    error[2] = false;

                    theSame[0] = true;
                    theSame[1] = true;
                    theSame[2] = true;
                    // make mistakes
                    if (i == sumErrorIndex) {
                        theSame[0] = false;
                        String old = String.format("%.2f,", total);

                        total = generateTotal();
                        sumErrorIndex = ++sumGapIndex * SUM_ERROR_PORTION + random.nextInt(SUM_ERROR_PORTION);
                        ++counterSUM;

                        if (old.equals(String.format("%.2f,", total)))
                            theSame[0] = true;

                        error[0] = true;
                    }
                    if (i == vatErrorIndex) {
                        theSame[1] = false;
                        String old = String.format("%.2f,", totalVAT);

                        totalVAT += 10_000.0 * random.nextDouble() + 0.1;
                        vatErrorIndex = ++vatGapIndex * VAT_ERROR_PORTION + random.nextInt(VAT_ERROR_PORTION);
                        ++counterVAT;

                        if (old.equals(String.format("%.2f,", totalVAT)))
                            theSame[1] = true;
                        error[1] = true;
                    }
                    if (i == innErrorIndex) {
                        theSame[2] = false;
                        String oldINN = customerINN;
                        String oldKPP = customerKPP;

                        customerINN = customerINN.substring(0, 4) +
                                Integer.toString(random.nextInt(9)) +
                                customerINN.substring(5);
                        customerKPP = customerKPP.substring(0, 5) +
                                Integer.toString(random.nextInt(9)) +
                                customerKPP.substring(6);
                        innErrorIndex = ++innGapIndex * INN_ERROR_PORTION + random.nextInt(INN_ERROR_PORTION);

                        ++counterINN;

                        if (oldINN.equals(customerINN) && oldKPP.equals(customerKPP))
                            theSame[2] = true;
                        error[2] = true;
                    }
                    if (error[0] || error[1] || error[2]) {
                        ++counterTotalErrorRows;

                        if (theSame[0] && theSame[1] && theSame[2])
                            ++counterTheSame;
                    }

                    if(error[0] && error[1])
                        ++counterTypes[0];
                    if(error[0] && error[2])
                        ++counterTypes[1];
                    if(error[1] && error[2])
                        ++counterTypes[2];
                    if (error[0] && error[1] && error[2])
                        ++counterTypes[3];

                    String customerStr = new StringBuilder()
                            .append(customerINN + ",").append(customerKPP + ",")
                            .append(sellerINN + ",").append(sellerKPP + ",")
                            .append(String.format("%.2f,", total))
                            .append(String.format("%.2f", totalVAT))
                            .toString();

                    writerSeller.append(sellerStr + "\n");
                    writerCustomer.append(customerStr + "\n");
                }
            }

            writerSeller.close();
            writerCustomer.close();

            System.out.println("All = " + String.valueOf(counterINN + counterSUM + counterVAT));
            System.out.println("INN = " + String.valueOf(counterINN) + " (" + INN_ERROR_PORTION + ")");
            System.out.println("SUM = " + String.valueOf(counterSUM) + " (" + SUM_ERROR_PORTION + ")");
            System.out.println("VAT = " + String.valueOf(counterVAT) + " (" + VAT_ERROR_PORTION + ")");
            System.out.println("TOTAL ERROR ROWS = " + String.valueOf(counterTotalErrorRows) +
                               " (" + String.valueOf(counterTotalErrorRows - counterTheSame) + ")");
            System.out.println("SUM and VAT = " + String.valueOf(counterTypes[0]));
            System.out.println("SUM and INN = " + String.valueOf(counterTypes[1]));
            System.out.println("VAT and INN = " + String.valueOf(counterTypes[2]));
            System.out.println("3 errors    = " + String.valueOf(counterTypes[3]));
            System.out.println("Error is not error = " + String.valueOf(counterTheSame));
        } catch (IOException e) {

        }
    }

    public static void main(String[] args) {
        generateData();
    }
}
